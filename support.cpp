#include "support.h"
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <iostream>

#define reset "\e[0m"
#define YEL "\e[0;33m"
#define GRN "\e[0;32m"
#define RED "\e[0;31m"

Node* node_new(char* str) {
    auto node = new Node{std::string(str), NULL};
    return node;
}

Node* node_new(const char* str) {
    auto node = new Node{std::string(str), NULL};
    return node;
}

Node* node_new_clone_str(char* str) {
    auto node = new Node{std::string(str), NULL};
    return node;
}

Node* node_new_clone_ch(char ch) {
    auto node = new Node{std::string() + ch, NULL};
    return node;
}

void node_print_list(Node* head) {
    while (head) {
        std::cout << head->str;
        head = head->next;
    }
}

void node_print_list_dbg(const char* name, Node* head) {
#ifndef NDEBUG
    fprintf(stderr, YEL "(debug) " GRN "`%s` " reset ": ", name);
    while (head) {
        std::cerr << head->str;
        head = head->next;
    }
    fputc('\n', stderr);
#endif
}

Node* node_get_tail(Node* head) {
    while (head->next)
        head = head->next;
    return head;
}

Node* concat(const size_t count, ...) {
    Node* first = NULL;
    Node* last  = NULL;
    va_list args;
    va_start(args, count);

    for (size_t i = 0; i < count; i++) {
        if (i == 0) {
            first = last = va_arg(args, Node*);
        } else {
            Node* new_last = va_arg(args, Node*);
            if (new_last == NULL) {
                continue;
            }

            while (last->next)
                last = last->next;

            Node* old_last = last;
            last           = new_last;
            old_last->next = last;
        }
    }
    va_end(args);

    return first;
}

void err(const char* str) {
    std::cerr << RED "Encountered error: " << str << "\n" << reset;
    exit(1);
}

void dbg(const char* str) {
#ifndef NDEBUG
    std::cerr << YEL "(debug) " reset << str << "\n";
#endif
}
