#ifndef SUPPORT_H
#define SUPPORT_H

#include <stdlib.h>

#include <string>

/**
 * @file
 * @brief Utilities to support the parser
 */

/** @struct _Node
 *  Node for building lists of strings
 *
 *  @var _Node::str
 *    Pointer to stored string
 *  @var _Node::next
 *    Pointer to next node (nullable)
 *  @var _Node::prev
 *    Poiner to previous node (nullable)
 */
struct Node {
    std::string str;
    Node* next = nullptr;
    Node(std::string str, Node* next = nullptr) : str(str), next(next) {}
};

/**
 * @brief Allocate space for a new \ref Node, and store the string (without
 * cloning, i.e. doesn't own the string)
 */
Node* node_new(char* str);
Node* node_new(const char* str);

/**
 * @brief Allocate space for a new \ref Node, clone the input string, and store
 * it.
 */
Node* node_new_clone_str(char* str);

/**
 * @brief Allocate space for a new \ref Node, create a new string, and store the
 * input character. it.
 */
Node* node_new_clone_ch(char ch);

/**
 * @brief Perform a walk from the given node and return when it sees tail. This
 * function must be used with caution, since it's possible to loop indefinitely
 * if there's a cycle.
 */
Node* node_get_tail(Node* head);

/**
 * @brief Concat `count` node lists and return the head. The first argument
 * indicates the number of lists to be concated.
 */
Node* concat(const size_t count, ...);

/**
 * @brief Print a list of nodes starting from `head`.
 */
void node_print_list(Node* head);

/**
 * @brief Debug print (to STDERR) a list of nodes, prepended by arbitrary name
 * text.
 *
 * For example, to print a node list with text "expr", do
 *
 * @code
 * node_print_list_dbg("expr", expr_head);
 * @endcode
 *
 * . The output will look like the following.
 *
 * @code
 * `expr` is <expr>b=<expr>2</expr></expr>
 * @endcode
 */
void node_print_list_dbg(const char* name, Node* head);

/**
 * @brief Print error message to STDERR and exit with status 1.
 */
void err(const char* str);

/**
 * @brief Print debug message to STDERR.
 */
void dbg(const char* str);

#endif /* ifndef SUPPORT_H */
